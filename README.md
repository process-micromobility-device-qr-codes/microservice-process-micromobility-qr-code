# PoC/Prototyp "Process micromobility QR code data"

This micro service is receiving the content of the QR codes, placed on micro mobility vehicles like e-scooters and rental bikes via API. The responses gives back the vendor name and QR code ID.

For example, if a city uses an incident reporting app, the app can automatically assign the correct vendor based on the QR code of the device. A user would scan the QR code with the app and would not have to manually select the vendor from e.g. a drop down field. Since the micro service is addressed via API, it can be used with any type of incident reporting system.

The micro service is based on NodeRED flows. It is just a proof of concept, any other technology or architecture will be fine too. ATM the API is provided by a hardcoded token into the flow. In production this should be done via an API gateway.

## Usage

* Install NodeRed
* Import flow files into NodeRED
* Send content of QR codes to API via GET
 * Add QR code content (param `url`)
 * Add access token to request (param `token`)
* Get response
 * Vendor name (`vendor_name`)
 * QR code ID (`qr_code_id`)

Example URL: `https://your-domain.tld/vendor/?token=your-secure-token&url=https://tier.app/AB123456`

Example call: `curl "https://your-domain.tld/vendor/?token=your-secure-token&url=https://tier.app/AB123456"`

Example response: `{"qr_code_id":"AB123456","vendor_name":"Tier"}`

## Future

This could be also used to retrieve data of the vehicle, via API request to the vendors GBFS APIs. This still has to be tested.

## To-Do

A lot! ;)

## License

MIT-License
